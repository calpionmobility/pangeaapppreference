/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.truckstop.pangea;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.util.Log;


/**
 * This class provides access to notifications on the device.
 *
 * Be aware that this implementation gets called on 
 * navigator.notification.{alert|confirm|prompt}, and that there is a separate
 * implementation in org.apache.cordova.CordovaChromeClient that gets
 * called on a simple window.{alert|confirm|prompt}.
 */
public class AppPreferences extends CordovaPlugin {
	
	private static final int COMMIT_FAILED = 2;
	private static final int NULL_VALUE = 3;
	private static final String PREF_NAME = "itstrucker";
	private static CordovaWebView cdvWebView;
	private static boolean watchChanges = false;

    /**
     * Constructor.
     */
    public AppPreferences() {
    }
	 public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArray of arguments for the plugin.
     * @param callbackContext   The callback context used when calling back into JavaScript.
     * @return                  True when the action was valid, false otherwise.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {


		String key = args.getString(0);

		if (action.equals("fetch")) {
			return this.fetchValueByKey(key, callbackContext);
		} else if (action.equals("fetchprofilevalues")) {
			return this.fetchProfileValues(key, callbackContext);
//			String value  = options.getString("value");
//			return this.storeValueByKey(key, type, value, callbackContext);
		} else if (action.equals("remove")) {
			return this.removeValueByKey(key, callbackContext);
		} else if (action.equals("fetchloginvalues")) {
			return this.fetchLoginValues(key, callbackContext);
		}
		// callbackContext.sendPluginResult(new PluginResult (PluginResult.Status.JSON_EXCEPTION));
		return false;

    }
	
	private boolean fetchValueByKey(final String key, final CallbackContext callbackContext) {
		cordova.getThreadPool().execute(new Runnable() {public void run() {

			//SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(cordova.getActivity());
			SharedPreferences sharedPrefs = cordova.getActivity().getSharedPreferences(PREF_NAME, cordova.getActivity().MODE_PRIVATE);
			String returnVal = null;
			if (sharedPrefs.contains(key)) {
				Object obj = sharedPrefs.getAll().get(key);
				String objClass = obj.getClass().getName();
				if (objClass.equals("java.lang.Integer") || objClass.equals("java.lang.Long")) {
					returnVal = obj.toString();
				} else if (objClass.equals("java.lang.Float") || objClass.equals("java.lang.Double")) {
					returnVal = obj.toString();
				} else if (objClass.equals("java.lang.Boolean")) {
					returnVal = (Boolean)obj ? "true" : "false";
				} else if (objClass.equals("java.lang.String")) {
					if (sharedPrefs.contains("_" + key + "_type")) {
						// here we have json encoded string
						returnVal = (String)obj;
					} else {
						String fakeArray = null;
						try {
							fakeArray = new JSONStringer().array().value((String)obj).endArray().toString();
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							callbackContext.error(0);
							return;
						}
						returnVal = fakeArray.substring(1, fakeArray.length()-1);
						// returnVal = new JSONStringer().value((String)obj).toString();
					}

				} else {
					Log.d("", "unhandled type: " + objClass);
				}
				// JSONObject jsonValue = new JSONObject((Map) obj);
				callbackContext.success(returnVal);
			} else {
				// Log.d("", "no value");
				callbackContext.success(returnVal);
				Log.d("", returnVal);
				// callbackContext.sendPluginResult(new PluginResult ());
			}

		}});

		return true;
	}

	private boolean removeValueByKey(final String key, final CallbackContext callbackContext) {
		cordova.getThreadPool().execute(new Runnable() { public void run() {

			//SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(cordova.getActivity());
			SharedPreferences sharedPrefs = cordova.getActivity().getSharedPreferences(PREF_NAME, cordova.getActivity().MODE_PRIVATE);

			if (sharedPrefs.contains(key)) {
				Editor editor = sharedPrefs.edit();
				editor.remove(key);
				if (sharedPrefs.contains("_" + key + "_type")) {
					editor.remove("_" + key + "_type");
				}

				if (editor.commit()) {
					callbackContext.success();
				} else {
					try {
						callbackContext.error(createErrorObj(COMMIT_FAILED, "Cannot commit change"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
			}

		}});

		return true;
	}
	private boolean fetchLoginValues(final String key, final CallbackContext callbackContext) {
		cordova.getThreadPool().execute(new Runnable() { public void run() {

			//SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(cordova.getActivity());
			SharedPreferences sharedPrefs = cordova.getActivity().getSharedPreferences(PREF_NAME, cordova.getActivity().MODE_PRIVATE);
			String returnVal = null;

			JSONObject obj = new JSONObject();
			try {
				obj.put("IsLoggedIn", sharedPrefs.getBoolean("IsLoggedIn", false));
				obj.put("handle", sharedPrefs.getString("handle", ""));
				obj.put("accountnumber", sharedPrefs.getString("accountnumber", ""));
				obj.put("password", sharedPrefs.getString("password", ""));
				callbackContext.success(obj.toString());

			}catch (Exception e){

			}
		}});

		return true;
	}

	private boolean fetchProfileValues(final String key, final CallbackContext callbackContext) {
		cordova.getThreadPool().execute(new Runnable() { public void run() {

			//SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(cordova.getActivity());
			SharedPreferences sharedPrefs = cordova.getActivity().getSharedPreferences(PREF_NAME, cordova.getActivity().MODE_PRIVATE);
			String returnVal = null;

			JSONObject obj = new JSONObject();
			try {
				obj.put("IsLoggedIn", sharedPrefs.getBoolean("IsLoggedIn", false));
				obj.put("handle", sharedPrefs.getString("handle", ""));
				obj.put("accountnumber", sharedPrefs.getString("accountnumber", ""));
				obj.put("password", sharedPrefs.getString("password", ""));
				obj.put("homebase", sharedPrefs.getString("homebase", ""));
				obj.put("countryindex", sharedPrefs.getInt("country", 0));
				obj.put("radius", sharedPrefs.getInt("radius", 125));
				obj.put("searchequipmentindex", sharedPrefs.getString("equipmentindex", ""));
				obj.put("searchTrailerOption", sharedPrefs.getString("traileroptions", ""));
				obj.put("loadsizetype", sharedPrefs.getInt("loadsizetype", 0));
				obj.put("hoursold", sharedPrefs.getInt("hoursold", 0));
				obj.put("postequipmentindex", sharedPrefs.getString("ppequipmentindex", ""));
				obj.put("postTrailerOption", sharedPrefs.getString("pptraileroptions", ""));
				obj.put("sorttype", sharedPrefs.getInt("sorttype", 0));
				obj.put("ppradius", sharedPrefs.getInt("ppradius", 125));
				obj.put("pploadsizetype", sharedPrefs.getInt("pploadsizetype", 2));
				obj.put("width", sharedPrefs.getString("width", ""));
				obj.put("length", sharedPrefs.getString("length", ""));
				obj.put("weight", sharedPrefs.getString("weight", ""));
				obj.put("mindist", sharedPrefs.getString("mindist", ""));
				obj.put("ratepermile", sharedPrefs.getString("ratepermile", ""));
				callbackContext.success(obj.toString());

			}catch (Exception e){

			}
		}});

		return true;
	}

	private JSONObject createErrorObj(int code, String message) throws JSONException {
		JSONObject errorObj = new JSONObject();
		errorObj.put("code", code);
		errorObj.put("message", message);
		return errorObj;
	}

    
}
